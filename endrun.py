#!/usr/bin/python3

import glob,os
#import pythonMidas as odb
import midas.client
from runlog import runlog

client = midas.client.MidasClient("endrun")
dir=client.odb_get('/Logger/Data dir')

rl = runlog()

latestJson = max(glob.iglob(dir + '/*.json'), key=os.path.getctime)

odb = rl.odbFromJson(latestJson)
rl.appendrun(odb)

client.disconnect()
