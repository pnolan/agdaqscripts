#!/bin/bash

if [[ -z ${MIDASSYS} ]]; then
    echo "'MIDASSYS' is not set"
else
    echo "Stopping run"
    $MIDASSYS/bin/odbedit -c stop
fi
