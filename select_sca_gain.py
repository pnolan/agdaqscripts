import numpy as np
from midas import client
import argparse

def get_pwb_odb():
    return(client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules"))

def get_scagain_odb(mod,sca):
    return(client.odb_get(f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_sca_gain/{mod:02d}[{sca}]"))

def set_scagain_odb(mod,sca,gain):
    client.odb_set(path=f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_sca_gain/{mod:02d}[{sca}]",
                   contents=gain,create_if_needed=False)

def get_pwb(x):
    pos=[int(x[0]/2),int(x[1]/2)]
    return pos[0]*MAX_PWB_COL+pos[1]    

def get_sca(x):
    if (x[0] % 2) == 0:
        col=True
    else:
        col=False
    if (x[1] % 2) == 0:
        axi=True
    else:
        axi=False
    
    if col and axi: return 0
    elif col and not axi: return 1
    elif not col and not axi: return 2
    elif not col and axi: return 3
    else: return -1
    


def draw_tpc(default_gain=0):
    tpc=''
    for c in range(15,-1,-1):
        tpc+=f'{c:2d}|'
        for a in range(16):
            imod=get_pwb([c,a])
            isca=get_sca([c,a])
            g=get_scagain_odb(imod,isca)
            if g<0: g=default_gain
            tpc+=f'   {g:2d}'
        tpc+='\n'
    #tpc+='___'
    tpc+='---'
    for a in range(16):
        #tpc+='_'*5
        tpc+='-'*5
    tpc+='\n'
    tpc+='  |'
    for a in range(16):
        tpc+=f'   {a:2d}'
    print(tpc)


import requests
def init_PWBs():
    cmd='init_pwb_all'
    address=''

    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':cmd,'args':address}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id': 0}

    url='http://localhost:8080?mjsonrpc'
    print('request', cmd, address)
    res=requests.post(url, json=payload, headers=headers).json()
    print('response', cmd, address, res['result']['reply'])



def reset():
    for i in range(64):
        for j in range(4):
            set_scagain_odb(i,j,-1)


ggain_list=[120,240,360,600]# fC
def gain_ratio():
    for i1,g1 in enumerate(ggain_list):
        for i2,g2 in enumerate(ggain_list):
            r=g1/g2
            print(f'{i1}/{i2} ratio: {r}')
'''
0/0 ratio: 1.0
0/1 ratio: 0.5
0/2 ratio: 0.33
0/3 ratio: 0.2
1/0 ratio: 2.0
1/1 ratio: 1.0
1/2 ratio: 0.67
1/3 ratio: 0.4
2/0 ratio: 3.0
2/1 ratio: 1.5
2/2 ratio: 1.0
2/3 ratio: 0.6
3/0 ratio: 5.0
3/1 ratio: 2.5
3/2 ratio: 1.67
3/3 ratio: 1.0
'''

MAX_PWB_COL=int(8)

if __name__=='__main__':
    parser=argparse.ArgumentParser(description='Write new per-SCA-gain configuration')
    parser.add_argument('--sca', type=int,
                        nargs=2,
                        help='SCA Sector and Row')
    parser.add_argument('--gain', type=int,
                        #nargs=1,
                        help='new gain',choices=range(-1,4))
    parser.add_argument('--test',action='store_true',help='show gain')
    parser.add_argument('--init',action='store_true',help='propagate changes')
    parser.add_argument('--reset',action='store_true',help='set global gain')
    args=parser.parse_args()

    client=client.MidasClient("select_sca_gain")
    pwbs=get_pwb_odb()
    #print(np.reshape(pwbs,(8,8)))

    gain_default=client.odb_get('/Equipment/CTRL/Settings/PWB/sca_gain')

    sel=args.sca
    if sel == None: sel=[0,0]

    ipwb=get_pwb(sel)
    isca=get_sca(sel)
    

    if args.test:
        print('just a test')
        print(f'default sca gain {gain_default} = {ggain_list[gain_default]}fC')
        gain_ratio()
    elif args.reset:
        print(f'reset to default sca gain {gain_default} = {ggain_list[gain_default]}fC')
        reset()
    elif args.init:
        print('initialize all PWBs')
        init_PWBs()
    else:
        old_gain=get_scagain_odb(ipwb,isca)
        if old_gain<0: gval=ggain_list[gain_default]
        else: gval=ggain_list[old_gain]
        print(f'{pwbs[ipwb]} at index {ipwb}, selected SCA {isca}, gain: {old_gain} = {gval}fC')
        new_gain=args.gain
        if new_gain == None: new_gain=-1
        if new_gain<0: gval=ggain_list[gain_default]
        else: gval=ggain_list[new_gain]
        print(f'Changing to {new_gain} = {gval}fC')
        set_scagain_odb(ipwb,isca,new_gain)
        

    draw_tpc(gain_default)

    client.disconnect()
