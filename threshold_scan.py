#!/usr/bin/python3

import socket
import sys
import requests
import json
import time
import argparse

import midas.client

import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

def SetROperiod1():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    client.odb_set(key,1)

def SetROperiod10():
    key='/Equipment/CTRL/Settings/PeriodScalers'
    client.odb_set(key,10)


def UpdateADC():
    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':'init_adc_all','args':''}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id':0}    
    url='http://localhost:8080?mjsonrpc'
    res=requests.post(url, json=payload, headers=headers).json()
    return res['result']['reply']

def SetADC16threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    client.odb_set(key,thr)
    UpdateADC()

def ReadADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    thr=client.odb_get(key)
    return int(thr)

def ResetADC16threshold():
    key='/Equipment/CTRL/Settings/ADC/adc16_threshold'
    client.odb_set(key,8192)
    UpdateADC()

def SetADC32threshold(thr):
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    client.odb_set(key,thr)
    UpdateADC()

def ReadADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    thr=client.odb_get(key)
    return int(thr)

def ResetADC32threshold():
    key='/Equipment/CTRL/Settings/ADC/adc32_threshold'
    client.odb_set(key,-8192)
    UpdateADC()

def ReadRates16():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=client.odb_get(key)
    rates=[float(r) for r in rates[24:32]]
    return rates

def ReadRates32():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=client.odb_get(key)
    rates=[float(r) for r in rates[32:48]]
    return rates

def AvgRates(adc):
    if adc == 'A16':
        rates = ReadRates16()
    elif adc == 'A32':
        rates = ReadRates32()
    else:
        print('Unknown adc type',adc)
    return sum(rates)/float(len(rates))

def ReadMLUrate():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=client.odb_get(key)
    return float(rates[67])

def Read4orMore():
    key='/Equipment/CTRL/Variables/scalers_rate'
    rates=client.odb_get(key)
    return float(rates[11])

def TimeAverageRate(scaler):
    time_avg=0.0
    Nmeas=10
    for r in range(Nmeas):
        if scaler == 'A16':
            time_avg += AvgRates('A16')
        elif scaler == 'A32':
            time_avg += AvgRates('A32')
        elif scaler == 'mlu':
            time_avg += ReadMLUrate()
        elif scaler == '4':
            time_avg += Read4orMore()
        else:
            print("don't know this scaler", scaler)
        time.sleep(2)
    return time_avg/float(Nmeas)

def Verbose():
    print('==================================================')
    print(f'ADC16 threshold: {ReadADC16threshold():6d} ADC16 avg. rates: {AvgRates("A16"):3.0f} Hz')
    print(f'ADC32 threshold: {ReadADC32threshold():6d} ADC32 avg. rates: {AvgRates("A32"):3.0f} Hz')
    print('==================================================')


def plot_scan(fname,slog=True,inter=False):
    thr,raw,trig=np.loadtxt(fname,
                           delimiter='\t', unpack=True)

    plot_title = 'ADC Threshold Scan'
    plot_label = 'Trigger Rate'
    if 'AW' in fname:
        plot_title += ' TPC AW'
        plot_label += ' MLU'
        x_ticks=range(-31000,1000,1000)
        plt.gca().invert_xaxis()
    elif 'BV' in fname:
        plot_title += 'Barrel Veto'
        plot_label += ' 4 or more'
        x_ticks=range(0,31000,1000)


    if slog:
        plt.semilogy(thr,raw,'or',label='ADC time avg.')
        plt.semilogy(thr,trig,'ob',label=plot_label)
    else:
        plt.plot(thr,raw,'or',label='ADC time avg.')
        plt.plot(thr,trig,'ob',label=plot_label)
    
    if inter:
        t=np.linspace(np.min(thr),np.max(thr),1000)
        fraw=interpolate.interp1d(thr,raw)
        plt.plot(t,fraw(t),'-r',label='interp. ADC time avg.')
        ftrg=interpolate.interp1d(thr,trig)
        plt.plot(t,ftrg(t),'-b',label='interp. '+plot_label)


    plt.title(plot_title)
    plt.xlabel('Threshold: ADC counts above pedestal')
    plt.xticks(x_ticks,fontsize=8)
    plt.ylabel('Counting Rate in Hz')
    plt.grid(True)
    plt.legend(loc='best')

    fig=plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    fig.tight_layout()
    fig.savefig(fname[:-4]+'.png')
    plt.show()


def BVscan():
    thr_list = [780, 820, 860, 900, 940, 980, 1050, 1100, 
                1200, 1400, 1600, 1800, 2000, 2500, 3000, 
                4000, 5000, 6000, 8000, 8192, 11000, 15000, 
                20000, 30000]

    print('Start...')

    fname=__file__[:-3]+'_BV_'+time.strftime("%Y%b%d_%H%M", time.localtime())+'.dat'
    print(fname)

    f=open(fname, 'w')
    for thr in thr_list:
        print(f'set: {thr:6d}', end=" ")
        
        # set the new threshold and wait for plateau
        SetADC16threshold(thr)
        time.sleep(10)
        
        # read the trigger rates and write them to file
        a16thr=ReadADC16threshold()
        a16rate=TimeAverageRate('A16')
        trig=Read4orMore()
            
        f.write('%d\t%1.1f\t%1.1f\n'%(a16thr,a16rate,trig))
        print(f'  read: {a16thr:6d}    ADC16 time avg.rate: {a16rate:1.1f} Hz   4 or more rate: {trig:1.1f} Hz')

    # reset the threshold
    ResetADC16threshold()
        
    f.close()
    print('Finished!')
    return fname

def AWscan():
    thr_list = [-700, -900, -1000, -1100,
                -1200, -1300, -1500, -2000, -2500, -3000, -3500,
                -4000, -4500, -5000, -6000, -7000, -8000, -8192, 
                -9500, -11000, -15000, -20000, -25000, -30000]

    # for high counting rate, fectrl scaler readout period 
    # has to be changed from 10 sec to 1 sec
    SetROperiod1()

    print('Start...')

    fname=__file__[:-3]+'_AW_'+time.strftime("%Y%b%d_%H%M", time.localtime())+'.dat'
    print(fname)

    f=open(fname, 'w')
    for thr in thr_list:
        print(f'set: {thr:6d}', end=" ")
        
        # set the new threshold and wait for plateau
        SetADC32threshold(thr)
        time.sleep(20)
        
        # read the trigger rates and write them to file
        a32thr=ReadADC32threshold()
        a32rate=TimeAverageRate('A32')
        trig=ReadMLUrate()
            
        f.write('%d\t%1.1f\t%1.1f\n'%(a32thr,a32rate,trig))
        print(f'  read: {a32thr:6d}    ADC32 time avg.rate: {a32rate:6.1f} Hz   mlu rate: {trig:6.1f} Hz')

    # reset the scaler readout period
    SetROperiod10()
    # reset the threshold
    ResetADC32threshold()
        
    f.close()
    print('Finished!')
    return fname
    

################################################################################

if __name__ == "__main__":

    parser=argparse.ArgumentParser()

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    parser.add_argument("adc", type=str, default='a32',
                        help="""Entrer
aw or AW or FMC or FMC32 or fmc or fmc32 or a32 or TPC
for scanning the anode wires threshold

Enter
a16 or A16 or bv or BV or bsc or BSC
for scanning the scintillating bars threshold
    """)
    args=parser.parse_args()

    client = midas.client.MidasClient("ThresholdScan")

    Verbose()

    print(f'Scanning {args.adc}...')

    if args.adc == 'aw' or args.adc == 'AW' or args.adc == 'FMC' or args.adc == 'FMC32' or args.adc == 'fmc' or args.adc == 'fmc32' or args.adc == 'a32' or args.adc == 'TPC':
        fname = AWscan()
    elif args.adc == 'a16' or args.adc == 'A16' or args.adc == 'bv' or args.adc == 'BSC' or args.adc == 'bsc' or args.adc == 'BSC':
        fname = BVscan()
    else:
        client.disconnect()
        sys.exit(f'Error: Unknown adc {args.adc}')

    plot_scan(fname)

    Verbose()

    client.disconnect()

