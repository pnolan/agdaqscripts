#!/usr/bin/python3

import requests
import json
import sys

if __name__=='__main__':

    addr='pwb01'
    if len(sys.argv)>1:
        addr=sys.argv[1]

    try:
        r=requests.get('http://'+addr+'/read_node?includeMods=y&includeVars=y&includeData=y')
        #print(json.dumps(r.json(), sort_keys=True, indent=4, separators=(',', ': ')))
        out=r.json()["module"]
        for mid in out:
            print(mid["id"],mid["name"])
            for vid in mid["var"]:
                print("\t",vid["key"],vid["d"])
            
    except Exception as e: 
        print(e)
