#!/bin/bash

JSON_ODB="/daq/alpha_data0/acapra/alphag/midasdata/run03873.json"

RUN_N=$(echo "$1" | grep -E [0-9]\{4,\})
#printf "%05d\n" ${RUN_N}

if [[ "${RUN_N}" != "" ]]; then
    ASK_ODB=$(printf "/daq/alpha_data0/acapra/alphag/midasdata/run%05d.json" ${RUN_N})
fi

if [[ -f ${ASK_ODB} ]]; then
    JSON_ODB=$ASK_ODB
fi

echo "Loading ${JSON_ODB}"

odbedit -s 100000000 -c "load ${JSON_ODB}"

odbedit -c "set \"/Logger/Message file\" /home/acapra/online/midas.log"
odbedit -c "set \"/Logger/Message dir\" /home/acapra/online/"
odbedit -c "set \"/Logger/History/FILE/History dir\" /home/acapra/online/history"
