#!/bin/bash

echo "This is a test message" > msg.txt
echo -e "Cheers,\nAndrea" >> msg.txt

ssh daq.triumf.ca /home/alphag/packages/elog/elog -h localhost -p 9081 -l alphag -v -a Author="Andrea" -a Type="Routine" -a Category="TPC" -a Subject="test\ msg\ with\ pic" -u acapra Hbar_2015 -m msg.txt -f "$HOME/Pictures/good_channel_sca.png" -x
rm msg.txt

# Reply
# elog -h https://daq.triumf.ca/elog-alphag/alphag/ -v -u acapra Hbar_2015 -f ../R904139/* -r 3358
# =======
